package core;
import java.awt.Desktop;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

import javax.naming.spi.DirectoryManager;

import net.sf.jasperreports.engine.JRBand;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

public class ReporterCore {
	
	SqlConnection conn;
	
	public ReporterCore(SqlConnection connection){
		conn = connection;
		java.io.File saveFile = new File(Vars.RSC_PATH);
		saveFile.mkdirs();
	}
	
	public void reportHelloWorld() throws FileNotFoundException{
		
		
		try {
			
			InputStream inputStream = new FileInputStream(Vars.RSC_PATH+"helloWorld.jrxml");
			
			JasperDesign jasperDesign = JRXmlLoader.load(inputStream);
			HashMap parameters = new HashMap();
			
			
			
			JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JREmptyDataSource());
			
			JasperExportManager.exportReportToPdfFile(	jasperPrint	, Vars.RSC_PATH+"helloWorldY£@H.pdf");
			System.out.println("sucCu£$$$$");
			
			
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	public void createReport(String fileName) throws FileNotFoundException{
		
		try {
			InputStream inputStream = new FileInputStream(fileName+".jrxml");
			
			JasperDesign jasperDesign;
			jasperDesign = JRXmlLoader.load(inputStream);
			HashMap parameters = new HashMap();
			
			JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,conn.getConnection());
			//JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, connection)
			
			JasperExportManager.exportReportToPdfFile(	jasperPrint	, Vars.RSC_PATH+fileName+".pdf");
			
			
			if(Desktop.isDesktopSupported()){
				try {
			        File myFile = new File(Vars.RSC_PATH+fileName+".pdf");
			        Desktop.getDesktop().open(myFile);
			    } catch (IOException ex) {
			        // no application registered for PDFs
			    }
			}
			
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	//availableProducts.jrxml
	public void reportAvailableProducts() throws FileNotFoundException{
		createReport("availableProducts");
	}
	
	public void reportUsers() throws FileNotFoundException{
		createReport("users");
	}
	
	public void reportSales() throws FileNotFoundException{
		createReport("salesSummary");
	}
	
	public void reportOneUser(String name) throws FileNotFoundException{
		HashMap<String	, Object> parameters =  new HashMap<String	, Object> ();
		parameters.put("username", name);
		createReportFromParameters(parameters, "salesSummary");
	}
	
	public void createReportFromParameters(HashMap<String	, Object>  parameters, String fileName) throws FileNotFoundException{
		try {
			InputStream inputStream = new FileInputStream(fileName+".jrxml");
			
			JasperDesign jasperDesign;
			jasperDesign = JRXmlLoader.load(inputStream);
			
			JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,conn.getConnection());
			//JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, connection)
			
			JasperExportManager.exportReportToPdfFile(	jasperPrint	, Vars.RSC_PATH+fileName+parameters.get("username")+".pdf");
			
			
			if(Desktop.isDesktopSupported()){
				try {
			        File myFile = new File(Vars.RSC_PATH+fileName+parameters.get("username")+".pdf");
			        Desktop.getDesktop().open(myFile);
			    } catch (IOException ex) {
			        // no application registered for PDFs
			    }
			}
			
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}
	

}
