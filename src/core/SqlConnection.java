package core;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.print.attribute.ResolutionSyntax;

import beans.Product;
import beans.User;

public class SqlConnection {
	Connection compiereAudaxis;

	String url = "jdbc:oracle:thin:@192.168.30.193:1521:audaxis";

	String user = "compiere";
	String password = "compiere";

	public SqlConnection() {

		compiereAudaxis = null;
		
		try {
			// initializing driver manager
			Class.forName("oracle.jdbc.OracleDriver");

			// creating connection
			compiereAudaxis = DriverManager.getConnection(url, user, password);
			System.out.println("connection created");

		} catch (Exception se) {
			se.printStackTrace();
		}
	}

	public Connection getConnection() {
		return compiereAudaxis;
	}

	public void setConnection(Connection compiereAudaxis) {
		this.compiereAudaxis = compiereAudaxis;
	}
	
public User getUser(String username, String password) throws SQLException{
		
		User user = new User();
		
		String statement = "SELECT * FROM USERS WHERE username='"+username+"' and password='"+password+"'";
		
		Statement sqlStatement = compiereAudaxis.createStatement();
		
		System.out.println(statement);
		
		ResultSet resultSet = sqlStatement.executeQuery(statement);
		
		if (resultSet.next()){
			user.setPassword(password);
			user.setUsername(username);
			return user;
		}
		
		return null;
	}

	
	public User[] getUsers() throws SQLException{
		
		String statement = "SELECT Count(*) As nbr FROM USERS";
		
		Statement sqlStatement = compiereAudaxis.createStatement();
		
		ResultSet resultSet = sqlStatement.executeQuery(statement);
		
		resultSet.next();
		
		int count = resultSet.getInt("nbr");
		
		User[] users = new User[count];
		statement = "SELECT * FROM USERS";
		resultSet = sqlStatement.executeQuery(statement);
		
		resultSet.next();
		for (int i =0; i<count ; i++){
			users[i] = new User();
			users[i].setPassword(resultSet.getString("PASSWORD"));
			users[i].setUsername(resultSet.getString("USERNAME"));
			resultSet.next();
		}
		
		return users;
	}
	
	public String getProductsString() throws SQLException{
		
		String statement = "SELECT * FROM PRODUCTS";
		
		Statement sqlStatement = compiereAudaxis.createStatement();
		
		ResultSet resultSet = sqlStatement.executeQuery(statement);
		
		String result = " products : ";
		boolean found = false;
		while(resultSet.next()){
			
			result +="\n"
					+ resultSet.getInt("IDP") + "  : " + resultSet.getString("name") + " : " + resultSet.getString("description");
			found = true;		
			
		}
		if(found) 
			return result;
		return "no products to show... sorry :p";
	}
	
	public Product[] getProducts() throws SQLException{
		
		Statement sqlStatement = compiereAudaxis.createStatement();
		
		String countStatement = "SELECT Count(*) AS nbr FROM PRODUCTS";
		
		ResultSet resultSet = sqlStatement.executeQuery(countStatement);
		
		resultSet.next();
		
		int count = resultSet.getInt("NBR");
		
		System.out.println(count);
		
		String statement = "SELECT * FROM PRODUCTS";
		
		resultSet = sqlStatement.executeQuery(statement);
		
		Product[] products= new Product[count];
		resultSet.next();
		for(int i=0; i<count ; i++){
			products[i] = new Product();
			products[i].setName(resultSet.getString("NAME"));
			products[i].setDescription(resultSet.getString("DESCRIPTION"));
			products[i].setImage("IMAGE");
			resultSet.next();
		}
		
		return products;
	}
	
	public boolean createAccount(String username, String password) throws SQLException{
		
		String statement = "INSERT INTO USERS(username, password) VALUES('"+username+"','"+password+"')";
		System.out.println(statement);
		Statement sqlStatement = compiereAudaxis.createStatement();
		if(getUser(username, password)==null)
		if (sqlStatement.executeUpdate(statement)>0) return true;
		return false;
	}
	
	public boolean removeUser(String username)throws SQLException{
		String statement = "DELETE FROM USERS WHERE USERS.USERNAME = '"+ username +"'";
		Statement sqlStatement = compiereAudaxis.createStatement();
		if (!username.equals(null))
			if(sqlStatement.executeUpdate(statement)==0) return false;
		return true;
	}
	
	public int addProduct(Product product) throws SQLException{
		String statement = "INSERT INTO PRODUCTS(name, description, image) VALUES('"+product.getName()+
				"', '"+product.getDescription()+"', '"+product.getImage()+"')";
		
		System.out.println(statement);
		
		Statement sqlStatement = compiereAudaxis.createStatement();
		
		return sqlStatement.executeUpdate(statement);
	}
	
	public String showUsers() throws SQLException{
		
		String statement = "SELECT * FROM USERS";
		Statement sqlStatement = compiereAudaxis.createStatement();
		
		ResultSet resultSet = sqlStatement.executeQuery(statement);
		
		String result = "user  / password";
		boolean found = false;
		while(resultSet.next()){
			result +="\n";
			result += " - "+ resultSet.getInt("IDU")+" :  "+ resultSet.getString("username")+ "  /  "+resultSet.getString("password");
			found = true;
		}
		if (found) return result;
		return "no users found ";
	}
	
	public int createSale(User user, int idproduct) throws SQLException{
		
		
		int idu = 0;
		
		String statement = "SELECT idu FROM USERS WHERE username='"+user.getUsername()+"' and password='"+user.getPassword()+"'";
		Statement sqlStatement = compiereAudaxis.createStatement();
		ResultSet resultSet = sqlStatement.executeQuery(statement);
		resultSet.next();
		idu = resultSet.getInt("IDU");
		
		statement = "INSERT INTO SALES (IDP, IDU, dateOf) VALUES ('"+idproduct+"','"+idu+"','12/12/2012')";
		System.out.println(statement);
		
		try{
			return sqlStatement.executeUpdate(statement);
		}catch (SQLException ex){
			return 0;
		}
	}

}
