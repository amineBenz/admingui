package uis;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneLayout;
import javax.swing.SpringLayout;

import beans.Product;
import beans.User;
import core.ReporterCore;
import core.SqlConnection;

import java.awt.event.ActionListener;
import java.io.Console;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;
import javax.swing.ListModel;
import javax.swing.JList;

public class MainFrame {

	private JFrame frame;
	
	SqlConnection connection = new SqlConnection();
	
	ReporterCore core = new ReporterCore(connection);
	
	Product currentProduct;
	
	Product[] listOfProducts;
	User[] users;
	
	JTextPane txtpnDescription = new JTextPane();
	JLabel lblName = new JLabel("name");

	private JList<String> list;
	
	JPanel productsPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame window = new MainFrame();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * @param core 
	 * @throws SQLException 
	 */
	public MainFrame() {
		try {
			connection.getUsers();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 * @throws SQLException 
	 */
	private void initialize() {
		   list = new JList<String>();
		try {
			listOfProducts = connection.getProducts();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		frame = new JFrame();
		frame.setBounds(100, 100, 730, 468);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		final JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 12, 315, 311);
		scrollPane.setLayout(new ScrollPaneLayout());
		
		
		
		createProductsButtons(scrollPane, frame.getContentPane());
		frame.getContentPane().add(scrollPane);
		
		JPanel panel = new JPanel();
		panel.setBounds(339, 12, 170, 125);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		final JCheckBox chckbxSalescheckbox = new JCheckBox("users :");
		chckbxSalescheckbox.setBounds(8, 8, 79, 23);
		panel.add(chckbxSalescheckbox);
		
		JLabel lblN = new JLabel("n°");
		lblN.setBounds(106, 12, 70, 15);
		panel.add(lblN);
		
		JLabel lblN_1 = new JLabel("n°");
		lblN_1.setBounds(106, 37, 70, 19);
		panel.add(lblN_1);
		
		final JCheckBox chckbxProducts = new JCheckBox("products :");
		chckbxProducts.setBounds(8, 37, 97, 23);
		panel.add(chckbxProducts);
		
		final JCheckBox chckbxSales = new JCheckBox("sales :");
		chckbxSales.setBounds(8, 64, 89, 23);
		panel.add(chckbxSales);
		
		JButton btnPrintReports = new JButton("print reports");
		btnPrintReports.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					
					if (chckbxProducts.isSelected())
						core.reportAvailableProducts();
					if (chckbxSales.isSelected())
						core.reportSales();
					if (chckbxSalescheckbox.isSelected())
						core.reportUsers();
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnPrintReports.setBounds(18, 95, 125, 25);
		panel.add(btnPrintReports);
		
		
		
		JLabel lblN_2 = new JLabel("n°");
		lblN_2.setBounds(106, 68, 70, 15);
		panel.add(lblN_2);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(12, 358, 704, 90);
		frame.getContentPane().add(scrollPane_1);
		
		JTextArea textArea = new JTextArea();
		scrollPane_1.setViewportView(textArea);
		
		
		txtpnDescription.setText("description");
		txtpnDescription.setBounds(339, 186, 170, 160);
		frame.getContentPane().add(txtpnDescription);
		
		lblName.setBounds(339, 149, 70, 15);
		frame.getContentPane().add(lblName);
		
		JButton btnHistory = new JButton("history");
		btnHistory.setBounds(410, 149, 99, 25);
		frame.getContentPane().add(btnHistory);
		
		final JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(521, 12, 195, 311);
		frame.getContentPane().add(scrollPane_2);
		
		scrollPane_2.setViewportView(list);
		
		try {
			createUsersList( scrollPane_2);
			
			JButton btnDelete = new JButton("add");
			btnDelete.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					frame.setEnabled(false);
					NewProd frame2 = new NewProd(connection);
//					while (frame2.getFrame().isVisible()){
//					
//					}
					try {
						listOfProducts = connection.getProducts();
					} catch (SQLException ex) {
						// TODO Auto-generated catch block
						ex.printStackTrace();
					}
					frame.setEnabled(true);
					productsPane = new JPanel();
					createProductsButtons(scrollPane, frame.getContentPane());
				}
			});
			btnDelete.setBounds(127, 327, 99, 25);
			frame.getContentPane().add(btnDelete);
			
			JButton btnHistory_1 = new JButton("history");
			btnHistory_1.setBounds(238, 327, 89, 25);
			frame.getContentPane().add(btnHistory_1);
			
			JButton button_1 = new JButton("delete");
			button_1.setBounds(22, 327, 89, 25);
			frame.getContentPane().add(button_1);
			
			JButton btnInfo = new JButton("info");
			btnInfo.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					String selected = (String) list.getSelectedValue();
					try {
						if (!selected.equals(null))
							core.reportOneUser(selected);
					} catch (FileNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			});
			btnInfo.setBounds(627, 327, 89, 25);
			frame.getContentPane().add(btnInfo);
			
			JButton btnAdd = new JButton("delete");
			btnAdd.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					String selected = (String) list.getSelectedValue();
					try {
						if (connection.removeUser(selected))
							createUsersList(scrollPane_2);
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			});
			btnAdd.setBounds(531, 327, 89, 25);
			frame.getContentPane().add(btnAdd);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	public void createUsersList( JScrollPane scrollPane) throws SQLException{
		
		DefaultListModel<String> model = new DefaultListModel<String>();
		
		
		
		users  = connection.getUsers();
		
		for (int i =0; i<users.length; i++){
			model.addElement(users[i].getUsername());
			System.out.println(model.get(i));
		}
		
		list = new JList();
		list.setModel((ListModel)model);
		scrollPane.setViewportView(list);
	}
	
	public void createProductsButtons(JScrollPane pane, Container container){
		pane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		pane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		
		productsPane = new JPanel();
		productsPane.setBorder(BorderFactory.createLineBorder(Color.red));
		//subPane.setPreferredSize(pane.getPreferredSize());
		System.out.println(pane.getPreferredSize());
		SpringLayout layout = new SpringLayout();
		productsPane.setLayout(new GridLayout(0, 2, 0, 0));
		//subPane.setLayout(layout);
		productsPane.setVisible(true);
		
		
		
		for (int i=0; i<listOfProducts.length; i++){
//			JButton butoone = new JButton(listOfProducts[i].getName());
			 final JButton butoone = new JButton(listOfProducts[i].getName());
			butoone.setVisible(true);
			butoone.setIcon(new ImageIcon("icon_x.png"));
			butoone.setPreferredSize(new Dimension(pane.getPreferredSize().width/2, 60));
			//butoone.setLayout(new SpringLayout());
			
			
			butoone.addActionListener(new ActionListener() {
				
				public void actionPerformed(ActionEvent e) {
					// TODO Auto-generated method stub
					changeProduct(butoone.getText());
				}
			});
			productsPane.add(butoone);
			productsPane.revalidate();
			
			
		}
		
		//subPane.setBorder(BorderFactory.createLineBorder(Color.red));
		//subPane.setPreferredSize(pane.getPreferredSize());
		//subPane.revalidate();
		//
		//container.add(subPane);
		pane.setViewportView(productsPane);
	}
	
	public void changeProduct(String name){
		for (int i = 0; i<listOfProducts.length; i++){
			if(listOfProducts[i].getName().equals(name)){
				currentProduct = listOfProducts[i];
				lblName.setText( currentProduct.getName());
				txtpnDescription.setText(currentProduct.getDescription());
			}
		}
	}
}
