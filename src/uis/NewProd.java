package uis;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;

import beans.Product;
import core.SqlConnection;

import javax.swing.JButton;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.awt.event.ActionEvent;

public class NewProd {

	private JFrame frame;
	public JFrame getFrame() {
		return frame;
	}

	private JTextField txtName;
	JTextArea textArea;
	
	SqlConnection connection;

	
	/**
	 * Create the application.
	 */
	public NewProd(SqlConnection connection) {
		this.connection = connection;
		initialize();
		frame.setVisible(true);
		frame.setFocusable(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		
		
		
		frame = new JFrame();
		frame.setBounds(100, 100, 470, 183);
		//frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		textArea = new JTextArea();
		textArea.setLineWrap(true);
		textArea.setBounds(12, 35, 456, 93);
		frame.getContentPane().add(textArea);
		
		txtName = new JTextField();
		txtName.setText("name");
		txtName.setBounds(12, 12, 456, 19);
		frame.getContentPane().add(txtName);
		txtName.setColumns(10);
		
		JButton btnAdd = new JButton("add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String name = txtName.getText();
				if (!(name.equals(null)) || !(name.equals(""))){
					String description = textArea.getText();
					Product product = new Product();
					product.setDescription(description);
					product.setName(name);
					product.setImage("none");
					
					try {
						connection.addProduct(product);
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					frame.dispose();
				}
			}
		});
		btnAdd.setBounds(182, 140, 117, 25);
		frame.getContentPane().add(btnAdd);
		
		
	}
}
